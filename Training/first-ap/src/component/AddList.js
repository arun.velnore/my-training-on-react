import React from 'react'

function AddList(props) {
    const handleClick=()=>{
        //if(this.props.HandleToDoList)
        //console.log(props.ToDoList)
        props.handleToDoList(props.toDoList)

    }
    return (
        <div>
            <p>{props.toDoList}</p>
            <button onClick={handleClick}>Remove ToDoList</button>
        </div>
    )
}

export default AddList
