import React,{useState,useEffect} from 'react'
import AddList from './AddList'
function UseState3() {
    const [item , setItem]= useState([]);
    const [newList,setNewList]=useState('');
    
    const addItem = () => { 
        console.log('before-',{item});
        if(newList.length>0){
            //const newAddList =
            setItem([...item ,  {
                id:item.length,
                value:<AddList toDoList={newList} handleToDoList={removeItem}/> 
                } ])
        }
        console.log('after-',{item});
        setNewList('');
    }
    const removeItem = (e)=>{
        console.log('   ',e)
        
        //setItem(items)
        

        console.log(item)
    }
    useEffect(() => {
        const cur=addItem
        return () => {
        }
    }, [item])
    

    const handleChange= (e)=>{
        setNewList(e.target.value)

    }
    
    return (
        <div>
            <input type='text' value={newList.trim()} onChange={handleChange}></input>
            <button onClick={addItem}>add TODOS list</button>
            
                {item.map(item=>
                    <li key={item.id}>{item.value}</li>
                )}
            
        </div>
    )
}


export default UseState3
