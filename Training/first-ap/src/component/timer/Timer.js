import React,{useState,useEffect} from 'react'

function Timer() {
    const [date, setDate] = useState(new Date())
    const tick= ()=>{
        setDate(new Date())
    }
    useEffect(() => {
        const timerID = setInterval(tick,1000);
        return ()=>{
            clearInterval(timerID)
        }
    },[date])
    
    return (
        <div>
            <h1>Hello, world!</h1>
            <h2>It is {date.toLocaleTimeString()}.</h2>
        </div>
    )
}

export default Timer

