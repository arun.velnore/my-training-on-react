import React,{useReducer} from 'react'

const init=0
const reducer = (state,action) =>{
    switch(action){
        case 'increment': 
            return state+1
        case 'decrement':
            return state-1
        case 'reset':
            return init
        default :
            return state
    }
}
function Reducer() {
    const [count,dispatch]=useReducer(reducer,init)
    return (

        <div>
            <div>count-{count}</div>
            <button onClick={()=>dispatch('increment')}>Increment</button>
            <button onClick={()=>dispatch('decrement')}>Decrement</button>
            <button onClick={()=>dispatch('reset')}>Reset</button>
        </div>
    )
}

export default Reducer
