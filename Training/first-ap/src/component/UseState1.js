import React,{useState} from 'react'

const UseState1 = () => {
    const [count, setCount] = useState(0);
    const incre5= ()=>{
        for(let i=0;i<5;i++)
            setCount(preCount=>preCount+1);
    }
    return (
        <div>
            <h1>{count}</h1>
            <button onClick={()=>setCount(0)}> reset</button>
            <button onClick={ () =>setCount(count+1)} > add</button>
            <button onClick={()=>setCount(count-1)} > sub</button>
            <button onClick={incre5}>Increment5</button>
        </div>
    );
}

export default UseState1

